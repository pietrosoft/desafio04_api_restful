//import { promises } from 'fs'
const fs = require('fs')

const Producto = require('./Producto.js')

module.exports = class Contenedor {

    constructor(nombre) {
        this.nombre = nombre
        this.productos = []

    }

    async delete() {
        try {
            await fs.unlink(this.nombre)
            console.log("Archivo : " + this.nombre + " Eliminado.")
        }
        catch (err) {
            //tratamiento de error
            console.log("No se puede eliminar el archivo : " + this.nombre)
        }
    }
    async deleteAll() {
        const data = []
        try {
            await fs.writeFile(this.nombre, JSON.stringify(this.productos, null, '\t'))
            console.log("Delete ALL -> Archivo : " + this.nombre + "  reseteado.")
        }
        catch (err) {
            console.log("No se puede grabar el archivo : " + this.nombre + err)
        }
    }

    async getAll() {
        try {
            const data = await fs.readFile(+this.nombre)
            if (data) {
                let arrayProductos = JSON.parse(data)

                return arrayProductos
            }
            else {
                console.log("No se encontro data en el archivo : " + this.nombre)
                return null
            }
        }
        catch (err) {
            console.log("No se encuentra el archivo : " + this.nombre + err)
            return null
        }

    }
    getAllSync() {
        try {
            const data = fs.readFileSync(this.nombre)
            if (data) {
                let arrayProductos = JSON.parse(data)

                return arrayProductos
            }
            else {
                console.log("No se encontro data en el archivo : " + this.nombre)
                return null
            }
        }
        catch (err) {
            console.log("No se encuentra el archivo : " + this.nombre + err)
            return null
        }

    }


    async getById(id) {
        try {
            this.productos = await this.getAll()
            let found = new Producto()
            if (this.productos.length > 0) {
                this.productos.forEach(element => {
                    if (element.id == id) {
                        found = new Producto(element.id, element.title, element.price, element.thumbnail)
                        //console.log(p)

                    }
                    //console.log("look up id:"+element.id )
                })
            }

            return found
        }
        catch (err) {
            console.log("No se puede leer el archivo : " + this.nombre + err)
            return -2
        }

    }

    getByIdSync(id) {
        try {
            this.productos = this.getAllSync()
            let found = null
            if (this.productos.length > 0) {
                this.productos.forEach(element => {
                    if (element.id == id) {
                        found = new Producto(element.id, element.title, element.price, element.thumbnail)
                        //console.log(p)
                    }
                    //console.log("look up id:"+element.id )
                })
            }

            return found
        }
        catch (err) {
            console.log("No se puede leer el archivo : " + this.nombre + err)
            return -2
        }

    }

    async deleteById(id = 0) {
        this.productos = await this.getAll()
        let i = 0;
        let found = false

        while (i < this.productos.length) {
            if (this.productos[i].id === id) {
                this.productos.splice(i, 1);
                console.log("eliminando id:" + id)
                found = true
            } else {
                ++i;
            }
        }
        if (found) {
            //GUAROD EL ARCHIVO
            try {
                await fs.writeFile(+this.nombre, JSON.stringify(this.productos, null, '\t'))
                console.log("Archivo : " + this.nombre + " Guardado.")
                return true
            }
            catch (err) {
                console.log("No se puede grabar el archivo : " + this.nombre + err)
            }
        } else {
            console.log("no se eliminaron registros con id:[" + id + "]")
        }
        return false;
    }

    deleteByIdSync(id = 0) {
        this.productos = this.getAllSync()
        let i = 0;
        let found = false

        while (i < this.productos.length && !found) {
            if (this.productos[i].id == id) {
                this.productos.splice(i, 1);
                console.log("eliminando id:" + id)
                found = true
            } else {
                //console.log("comparo id:"+this.productos[i].id + "con : "+ id);
            }
            ++i;
        }
        if (found) {
            //GUARDo EL ARCHIVO
            try {
                fs.writeFileSync(this.nombre, JSON.stringify(this.productos, null, '\t'))
                console.log("Archivo : " + this.nombre + " Guardado.")
                return true
            }
            catch (err) {
                console.log("No se puede grabar el archivo : " + this.nombre + err)
            }
        } else {
            console.log("no se eliminaron registros con id:[" + id + "]")
        }

        return false;
    }



    async deleteAll() {
        //GUAROD EL ARCHIVO
        try {
            this.productos = []
            await fs.writeFile(this.nombre, JSON.stringify(this.productos, null, '\t'))
            console.log("Archivo : " + this.nombre + " Guardado.")
            return true
        }
        catch (err) {
            console.log("No se puede grabar el archivo : " + this.nombre + err)
        }

        return false;
    }

    updateByIdSync(id = 0, producto) {
        this.productos = this.getAllSync()
        //let producto = this.getByIdSync(id)
        let i = 0;
        let found = false

        if (producto) {
            while (i < this.productos.length && !found) {
                if (this.productos[i].id == id) {
                    producto.id=id;
                    this.productos[i] = producto
                    console.log("actualizo id:" + id)
                    found = true
                } else {
                    //console.log("comparo id:"+this.productos[i].id + "con : "+ id);
                }
                ++i;
            }
            if (found) {
                //GUARDo EL ARCHIVO
                try {
                    fs.writeFileSync(this.nombre, JSON.stringify(this.productos, null, '\t'))
                    console.log("Archivo : " + this.nombre + " Guardado.")
                    return true
                }
                catch (err) {
                    console.log("No se puede grabar el archivo : " + this.nombre + err)
                    return false
                }
            } else {
                console.log("no se actualizaron registros con id:[" + id + "]")
                return false
            }

        }
        return false;
    }

    async save(producto) {
        if (producto) {
            this.productos = await this.getAll()
            if (this.productos == null) {
                this.productos = []
            }
            let id = 0
            if (this.productos.length > 0) {
                let lastId = this.productos[this.productos.length - 1].id
                //console.log("last id:"+lastId)
                id = lastId + 1
            } else {
                id = 1
            }
            console.log("nuevo id : " + id)
            producto.id = id
            this.productos.push(producto)
        }

        try {
            await fs.writeFile(this.nombre, JSON.stringify(this.productos, null, '\t'))
            console.log("Archivo : " + this.nombre + " Guardado.")
            return producto.id
        }
        catch (err) {
            console.log("No se puede grabar el archivo : " + this.nombre + err)
            return -1
        }
    }

    saveSync(producto) {
        if (producto) {
            this.productos = this.getAllSync()
            if (this.productos == null) {
                this.productos = []
            }
            let id = 0
            if (this.productos.length > 0) {
                let lastId = this.productos[this.productos.length - 1].id
                //console.log("last id:"+lastId)
                id = lastId + 1
            } else {
                id = 1
            }
            console.log("nuevo id : " + id)
            producto.id = id
            this.productos.push(producto)
        }

        try {
            fs.writeFileSync(this.nombre, JSON.stringify(this.productos, null, '\t'))
            console.log("Archivo : " + this.nombre + " Guardado.")
            return producto
        }
        catch (err) {
            console.log("No se puede grabar el archivo : " + this.nombre + err)
            return -1
        }
    }


}


