const express = require('express');
const Contenedor = require('./classes/Contenedor.js');

//Defino variables
let contenedor = new Contenedor('productos.json');

// creo una app de tipo express
const app = express();
// incorporo el router
const router = express.Router();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//defino ruta base
app.use('/api', router);

// indico donde estan los archivos estaticos
app.use(express.static('public'));

const puerto = 8080;

// pongo a escuchar el servidor en el puerto indicado
const server = app.listen(puerto, () => {
    //console.log(`servidor escuchando en http://localhost:${puerto}`);
    console.log("Iniciando servidor")
    //console.log(obj_items)
    console.log("servidor inicializado en puerto:" + server.address().port)
});


// en caso de error, avisar
server.on('error', error => {
    console.log('error en el servidor:', error);
});
router.get('/', (req, res) => {
    res.sendFile('index');
});

//TEST
router.get('/test', (req, res) => {
    res.send('get recibido!');
});

router.post('/test', (req, res) => {
    res.send('post recibido!')
});

//LISTAR
router.get('/productos', (req, res) => {
    //listo todos los productos
    let result = contenedor.getAllSync();
    if (result) {
        res.json(result);
    } else {
        res.status(500).json({ error: 'no se pueden obtener los productos' });
    }

});

//LISTAR UNO
router.get('/productos/:id', (req, res) => {
    //LISTO SOLO 1 Elemento    
    let result = contenedor.getByIdSync(req.params.id)
    if (result) {
        res.json(result);
    } else {
        res.status(500).json({ error: 'producto no encontrado' });
    }
});

// CARGA PRODUCTOS
router.post('/productos', (req, res) => {

    if (Array.isArray(req.body)) {
        let added_products = [];
        let json_array = req.body.map(JSON.stringify);
        json_array.forEach(it => {
            let producto = JSON.parse(it);
            let response = contenedor.saveSync(producto)
            added_products.push(response);

        });
        if (added_products.length > 0) {
            res.json(added_products)
        }
        else {
            res.status(500).json({ resultado: 'No se puede guardar el producto encontrado' })
        }

    } else {
        let result = contenedor.saveSync(req.body)
        if (result) {
            res.json(result)
        } else {
            res.status(500).json({ resultado: 'No se puede guardar el producto encontrado' })
        }

    }
})

// ACTUALIZA PRODUCTOS
router.put('/productos/:id', (req, res) => {
    let result = contenedor.updateByIdSync(req.params.id, req.body);
    if (result) {
        res.json({ resultado: 'id: ' + req.params.id + ' actualizado' })
    } else {
        res.status(500).json({ error: 'producto no encontrado' });
    }
    //res.json(productos.updateProduct(req.params.id,req.body.title, req.body.price, req.body.thumbnail))

})

// ELIMINA PRODUCTOS
router.delete('/productos/:id', (req, res) => {
    // res.json(productos.deleteProduct(req.params.id,))
    let result = contenedor.deleteByIdSync(req.params.id);
    if (result) {
        res.json({ resultado: 'id: ' + req.params.id + ' eliminado' })
    } else {
        res.status(500).json({ resultado: 'id:' + req.params.id + ' No enocontrado' })
    }

})